#include "Source.h"


#ifndef Straight_H
#define Straight_H

class Straight
{
public:
	Straight() {
		aValue = 1;
		bValue = 0;
	};
	~Straight() {
	};

	void estimateHorizontalStraight(vector <KeyPoint> keyPoints);
	void estimateVerticalStraight(vector <KeyPoint> keyPoints);

	double getAValue();
	double getBValue();

private:
	double aValue;
	double bValue;
};

#endif /*Straight_H*/