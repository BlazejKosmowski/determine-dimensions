#include "Source.h"

#ifndef ObjectProportion_H
#define ObjectProportion_H
class ObjectProportion
{
public:
	ObjectProportion()
	{
		topDownDistance = 0.0;
		leftRightDistance = 0.0;
	};
	~ObjectProportion()
	{
	};

	double calculateDistance(double aSide, double bSide);

	void setTopDownDistance(double distance);
	void setLeftRightDistance(double distance);

	double getTopDownDistance();
	double getLeftRightDistance();

	double objectHeightPercent(int rowsAmount);
	double objectWidthPercent(int colsAmount);

	double detectVerticalLinesAndCalculateWidth(Mat matImage);
	double detectHorizontalLinesAndCalculateHeight(Mat matImage);
private:
	double topDownDistance;
	double leftRightDistance;
};

#endif /*ObjectProportion_H*/