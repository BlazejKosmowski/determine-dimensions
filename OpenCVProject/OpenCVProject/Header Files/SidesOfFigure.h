#include "Source.h"


#ifndef SidesOfFigure_H
#define SidesOfFigure_H

class SidesOfFigure
{
public:
	SidesOfFigure() {
	};
	~SidesOfFigure() {
	};

	void setLeftVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfRows);
	void setRightVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfRows);
	void setTopVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfCols);
	void setDownVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfCols);

	double meanValueHorizontalKeyPointElement(vector<KeyPoint> keyPoints, int amountOfCols);
	double meanValueVerticalKeyPointElement(vector<KeyPoint> keyPoints, int amountOfRows);

	vector <KeyPoint> filterLeftKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue);
	vector <KeyPoint> selectLeftKeyPointVector(vector<KeyPoint> keyPoints, int amountOfCols);

	vector <KeyPoint> filterRightKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue);
	vector <KeyPoint> selectRightKeyPointVector(vector<KeyPoint> keyPoints, int amountOfCols);

	vector <KeyPoint> filterTopKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue);
	vector <KeyPoint> selectTopKeyPointVector(vector<KeyPoint> keyPoints, int amountOfRows);

	vector <KeyPoint> filterDownKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue);
	vector <KeyPoint> selectDownKeyPointVector(vector<KeyPoint> keyPoints, int amountOfRows);

	vector <KeyPoint> getLeftVectorOfKeyPoints();
	vector <KeyPoint> getRightVectorOfKeyPoints();
	vector <KeyPoint> getTopVectorOfKeyPoints();
	vector <KeyPoint> getDownVectorOfKeyPoints();

	// getCorrectSidesOfFigureFromVectorOfKeyPoints(vector <KeyPoint> keyPoints);
private:
	vector <KeyPoint> left;
	vector <KeyPoint> right;
	vector <KeyPoint> top;
	vector <KeyPoint> down;
};



#endif /*SidesOfFigure_H*/