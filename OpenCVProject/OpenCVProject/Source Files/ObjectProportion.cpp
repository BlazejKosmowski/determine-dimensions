#include "..\Header Files\Source.h"
using namespace std;

void ObjectProportion::setTopDownDistance(double distance)
{
	topDownDistance = distance;
}

void ObjectProportion::setLeftRightDistance(double distance)
{
	leftRightDistance = distance;
}

double ObjectProportion::getTopDownDistance()
{
	return topDownDistance;
}

double ObjectProportion::getLeftRightDistance()
{
	return leftRightDistance;
}

double ObjectProportion::objectHeightPercent(int rowsAmount)
{
	return (topDownDistance / rowsAmount) * 100;
}

double ObjectProportion::objectWidthPercent(int colsAmount)
{
	return (leftRightDistance / colsAmount) * 100;
}

double ObjectProportion::calculateDistance(double aSide, double bSide)
{
	return abs(aSide - bSide);
}


double ObjectProportion::detectVerticalLinesAndCalculateWidth(Mat matImage) {
	Mat binaryMatImage;
	adaptiveThreshold(~matImage, binaryMatImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, -2);
	Mat verticalMat = binaryMatImage.clone();

	vector <KeyPoint> keyPoints;
	for (int i = verticalMat.rows / 60; i < verticalMat.rows; i++) { //TODO: poprawi� warunek.
		if (verticalMat.rows % i == 0) {
			Mat verticalStructreMat = getStructuringElement(MORPH_RECT, Size(1, i));

			erode(verticalMat, verticalMat, verticalStructreMat, Point(-1, -1));
			dilate(verticalMat, verticalMat, verticalStructreMat, Point(-1, -1));

			Canny(verticalMat, verticalMat, 33, 100, 3);
			FAST(verticalMat, keyPoints, 15, true);
			Mat resultKeyPoints;
			if (keyPoints.capacity() > 1) {
				cout << verticalMat.rows / i << endl;
				drawKeypoints(verticalMat, keyPoints, resultKeyPoints, 156);
				/*for (;;)
				{
					resize(resultKeyPoints, resultKeyPoints, Size(1300, 900));
					namedWindow("resultKeyPoints1", CV_WINDOW_AUTOSIZE);
					imshow("resultKeyPoints1", resultKeyPoints);
					if (waitKey(30) >= 0) break;
				}*/
				break;
			}
		}
	}
	

	SidesOfFigure sideOfFigure;
	sideOfFigure.setLeftVectorOfKeyPoints(keyPoints, verticalMat.rows);
	sideOfFigure.setRightVectorOfKeyPoints(keyPoints, verticalMat.rows);

	Straight leftStraight;
	leftStraight.estimateVerticalStraight(sideOfFigure.getLeftVectorOfKeyPoints());
	Straight rightStraight;
	rightStraight.estimateVerticalStraight(sideOfFigure.getRightVectorOfKeyPoints());


	double XValue;
	double prevXValue = leftStraight.getAValue();
	for (int i = 1; i < matImage.rows; i++) {
		XValue = leftStraight.getAValue();
		line(matImage, Point(prevXValue, i - 1), Point(XValue, i), CV_RGB(255, 25, 60), 20);
		prevXValue = XValue;
	}

	prevXValue = rightStraight.getAValue();
	for (int i = 1; i < matImage.rows; i++) {
		XValue = rightStraight.getAValue();
		line(matImage, Point(prevXValue, i - 1), Point(XValue, i), CV_RGB(255, 25, 60), 20);
		prevXValue = XValue;
	}

	/*for (;;)
	{
 		resize(matImage, matImage, Size(500,500));
		namedWindow("matImage1", CV_WINDOW_AUTOSIZE);
		imshow("matImage1", matImage);
		if (waitKey(30) >= 0) break;
	}*/


	ObjectProportion objectProportion;
	double leftRightDistance = objectProportion.calculateDistance(leftStraight.getAValue(), rightStraight.getAValue());
	objectProportion.setLeftRightDistance(leftRightDistance);

	double leftRightPercentDistance = objectProportion.objectWidthPercent(verticalMat.cols);

	cout << "Szeroko�� objektu na obrazku w procentach: " << leftRightPercentDistance << " %." << endl;
/*
	drawVerticalLine(leftStraight, verticalMat);
	drawVerticalLine(rightStraight, verticalMat);
*/
	//imwrite("C:/Users/B�a�ej/Documents/inzynierka/OpenCVProject/OpenCVProject/Images/verticalMatresultKeyPointsCanny.bmp", resultKeyPoints);
	//imwrite("C:/Users/B�a�ej/Documents/inzynierka/OpenCVProject/OpenCVProject/Images/verticalMatresultKeyPointsCannyVerticalLines.bmp", resultKeyPoints);
	return objectProportion.leftRightDistance;
}

double ObjectProportion::detectHorizontalLinesAndCalculateHeight(Mat matImage) {
	Mat binaryMatImage;
	adaptiveThreshold(~matImage, binaryMatImage, 255, CV_ADAPTIVE_THRESH_MEAN_C, THRESH_BINARY, 15, -2);
	Mat horizontalMat = binaryMatImage.clone();


	vector <KeyPoint> keyPoints;
	
	for (int i = horizontalMat.cols / 60; i < horizontalMat.cols; i++) {  //TODO: poprawi� warunek.
		if (horizontalMat.cols % i == 0) {
			Mat horizontalStructreMat = getStructuringElement(MORPH_RECT, Size(i, 1));

			erode(horizontalMat, horizontalMat, horizontalStructreMat, Point(-1, -1));
			dilate(horizontalMat, horizontalMat, horizontalStructreMat, Point(-1, -1));

			Canny(horizontalMat, horizontalMat, 33, 100, 3);
			FAST(horizontalMat, keyPoints, 15, true);
			Mat resultKeyPoints;
			if (keyPoints.capacity() > 1) {
				cout << horizontalMat.cols / i << endl;
				drawKeypoints(horizontalMat, keyPoints, resultKeyPoints, 156);
				/*for (;;)
				{
					resize(resultKeyPoints, resultKeyPoints, Size(1300, 900));
					namedWindow("resultKeyPoints2", CV_WINDOW_AUTOSIZE);
					imshow("resultKeyPoints2", resultKeyPoints);
					if (waitKey(30) >= 0) break;
				}*/
				break;
			}
		}
	}


	SidesOfFigure sideOfFigure;
	sideOfFigure.setTopVectorOfKeyPoints(keyPoints, horizontalMat.cols);
	sideOfFigure.setDownVectorOfKeyPoints(keyPoints, horizontalMat.cols);

	Straight topStraight;
	topStraight.estimateHorizontalStraight(sideOfFigure.getTopVectorOfKeyPoints());
	Straight downStraight;
	downStraight.estimateHorizontalStraight(sideOfFigure.getDownVectorOfKeyPoints());

	double yValue;
	double prevYValue = topStraight.getBValue();
	for (int i = 1; i < matImage.cols; i++) {
		yValue = topStraight.getBValue();
		line(matImage, Point(i - 1, prevYValue), Point(i, yValue), CV_RGB(255, 25, 60), 20);
		prevYValue = yValue;
	}

	yValue;
	prevYValue = downStraight.getBValue();
	for (int i = 1; i < matImage.cols; i++) {
		yValue = downStraight.getBValue();
		line(matImage, Point(i - 1, prevYValue), Point(i, yValue), CV_RGB(255, 25, 60), 20);
		prevYValue = yValue;
	}

	/*for (;;)
	{
		resize(matImage, matImage, Size(500, 500));
		namedWindow("matImage2", CV_WINDOW_AUTOSIZE); 
		imshow("matImage2", matImage);
		if (waitKey(30) >= 0) break;
	}*/

	ObjectProportion objectProportion;
	double topDownDistance = objectProportion.calculateDistance(topStraight.getBValue(), downStraight.getBValue());
	objectProportion.setTopDownDistance(topDownDistance);

	double topDownPercentDistance = objectProportion.objectHeightPercent(horizontalMat.rows);

	cout << "Wysoko�� objektu na obrazku  procentach: " << topDownPercentDistance << " %." << endl;
	/*
		drawHorizontalLine(topStraight, horizontalMat);
		drawHorizontalLine(downStraight, horizontalMat);
	*/
	//imwrite("C:/Users/B�a�ej/Documents/inzynierka/OpenCVProject/OpenCVProject/Images/horizontalMatresultKeyPointsCanny.bmp", resultKeyPoints);
	//imwrite("C:/Users/B�a�ej/Documents/inzynierka/OpenCVProject/OpenCVProject/Images/horizontalMatresultKeyPointsCannyHorizonatlLines.bmp", resultKeyPoints);

	return objectProportion.topDownDistance;
}