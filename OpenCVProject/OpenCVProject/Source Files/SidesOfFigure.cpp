#include "../Header Files/Source.h"

vector <KeyPoint> SidesOfFigure::getLeftVectorOfKeyPoints() {
	return left;
}

vector <KeyPoint> SidesOfFigure::getRightVectorOfKeyPoints() {
	return right;
}

vector <KeyPoint> SidesOfFigure::getTopVectorOfKeyPoints() {
	return top;
}

vector <KeyPoint> SidesOfFigure::getDownVectorOfKeyPoints() {
	return down;
}

void SidesOfFigure::setRightVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfCols) {
	try {
		vector<KeyPoint> selectedRightKeyPointVector = selectRightKeyPointVector(keyPoints, amountOfCols);

		right = selectRightKeyPointVector(selectedRightKeyPointVector, amountOfCols);
	}
	catch (Exception e) {
		throw Exception(e);
	}
}

void SidesOfFigure::setLeftVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfCols) {
	try {
		vector<KeyPoint> selectedLeftKeyPointVector = selectLeftKeyPointVector(keyPoints, amountOfCols);

		left = selectLeftKeyPointVector(selectedLeftKeyPointVector, amountOfCols);
	}
	catch (Exception e) {
		throw Exception(e);
	}
}

void SidesOfFigure::setTopVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfRows) {
	try {
		vector<KeyPoint> selectedTopKeyPointVector = selectTopKeyPointVector(keyPoints, amountOfRows);

		top = selectTopKeyPointVector(selectedTopKeyPointVector, amountOfRows);
	}
	catch (Exception e) {
		throw Exception(e);
	}
}

void SidesOfFigure::setDownVectorOfKeyPoints(vector <KeyPoint> keyPoints, int amountOfRows) {
	try {
		vector<KeyPoint> selectedDownKeyPointVector = selectDownKeyPointVector(keyPoints, amountOfRows);

		down = selectDownKeyPointVector(selectedDownKeyPointVector, amountOfRows);
	}
	catch (Exception e) {
		throw Exception(e);
	}
}

double SidesOfFigure::meanValueHorizontalKeyPointElement(vector<KeyPoint> keyPoints, int amountOfCols)
{
	double maxValue = 0.0;
	double meanValue = 0.0;
	double minValue = amountOfCols;

	for (int i = 0; i < keyPoints.capacity(); i++) {
		if (keyPoints[i].pt.x > maxValue) {
			maxValue = keyPoints[i].pt.x;
		}
		else if (keyPoints[i].pt.x < minValue) {
			minValue = keyPoints[i].pt.x;
		}
	}

	meanValue = (maxValue + minValue) / 2;

	return meanValue;
}

double SidesOfFigure::meanValueVerticalKeyPointElement(vector<KeyPoint> keyPoints, int amountOfRows)
{
	double maxValue = 0.0;
	double meanValue = 0.0;
	double minValue = amountOfRows;

	for (int i = 0; i < keyPoints.capacity(); i++) {
		if (keyPoints[i].pt.y > maxValue) {
			maxValue = keyPoints[i].pt.y;
		}
		if (keyPoints[i].pt.y < minValue) {
			minValue = keyPoints[i].pt.y;
		}
	}

   	meanValue = (maxValue + minValue) / 2;

	return meanValue;
}

vector<KeyPoint> SidesOfFigure::filterLeftKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue)
{
	vector<KeyPoint> filteredLeftKeyPointVector;

	for (int i = 0; i < keyPoints.capacity(); i++) {
		if (keyPoints[i].pt.x <= meanKeyPointVectorValue) {
			filteredLeftKeyPointVector.push_back(keyPoints[i]);
		}
	}

	return filteredLeftKeyPointVector;
}

vector<KeyPoint> SidesOfFigure::selectLeftKeyPointVector(vector<KeyPoint> keyPoints, int amountOfCols)
{
	double meanKeyPointVectorValue = meanValueHorizontalKeyPointElement(keyPoints, amountOfCols);

	vector<KeyPoint> filteredLeftKeyPointVector = filterLeftKeyPointVector(keyPoints, meanKeyPointVectorValue);

	return filteredLeftKeyPointVector;
}

vector<KeyPoint> SidesOfFigure::filterRightKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue)
{
	vector<KeyPoint> filteredRightKeyPointVector;

	for (int i = 0; i < keyPoints.capacity(); i++) {
		if (keyPoints[i].pt.x >= meanKeyPointVectorValue) {
			filteredRightKeyPointVector.push_back(keyPoints[i]);
		}
	}

	return filteredRightKeyPointVector;
}

vector<KeyPoint> SidesOfFigure::selectRightKeyPointVector(vector<KeyPoint> keyPoints, int amountOfCols)
{
	double meanKeyPointVectorValue = meanValueHorizontalKeyPointElement(keyPoints, amountOfCols);

	vector<KeyPoint> filteredRightKeyPointVector = filterRightKeyPointVector(keyPoints, meanKeyPointVectorValue);

	return filteredRightKeyPointVector;
}

vector<KeyPoint> SidesOfFigure::filterTopKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue)
{
	vector<KeyPoint> filteredTopKeyPointVector;

	for (int i = 0; i < keyPoints.capacity(); i++) {
		if (keyPoints[i].pt.y >= meanKeyPointVectorValue) {
			filteredTopKeyPointVector.push_back(keyPoints[i]);
		}
	}

	return filteredTopKeyPointVector;
}

vector<KeyPoint> SidesOfFigure::selectTopKeyPointVector(vector<KeyPoint> keyPoints, int amountOfRows)
{
	double meanKeyPointVectorValue = meanValueVerticalKeyPointElement(keyPoints, amountOfRows);

	vector<KeyPoint> filteredTopKeyPointVector = filterTopKeyPointVector(keyPoints, meanKeyPointVectorValue);

	return filteredTopKeyPointVector;
}

vector<KeyPoint> SidesOfFigure::filterDownKeyPointVector(vector<KeyPoint> keyPoints, double meanKeyPointVectorValue)
{
	vector<KeyPoint> filteredDownKeyPointVector;

	for (int i = 0; i < keyPoints.capacity(); i++) {
		if (keyPoints[i].pt.y <= meanKeyPointVectorValue) {
			filteredDownKeyPointVector.push_back(keyPoints[i]);
		}
	}

	return filteredDownKeyPointVector;
}

vector<KeyPoint> SidesOfFigure::selectDownKeyPointVector(vector<KeyPoint> keyPoints, int amountOfRows)
{
	double meanKeyPointVectorValue = meanValueVerticalKeyPointElement(keyPoints, amountOfRows);

	vector<KeyPoint> filteredDownKeyPointVector = filterDownKeyPointVector(keyPoints, meanKeyPointVectorValue);

	return filteredDownKeyPointVector;
}
