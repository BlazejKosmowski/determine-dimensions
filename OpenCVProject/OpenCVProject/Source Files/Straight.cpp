#include "../Header Files/Source.h"

void Straight::estimateHorizontalStraight(vector <KeyPoint> keyPoints) {
	try {
		float sumOfBValue = 0;
		for (int i = 0; i < keyPoints.capacity() && i < 500; i++) {
			sumOfBValue += keyPoints[i].pt.y;
		}
		if (keyPoints.capacity() < 500) {
			bValue = sumOfBValue / (keyPoints.capacity());
		}
		else {
			bValue = sumOfBValue / 500;
		}
	}
	catch (Exception e) {
		throw e;
	}
}

void Straight::estimateVerticalStraight(vector<KeyPoint> keyPoints)
{
	try {
		float sumOfAValue = 0;
		for (int i = 0; i < keyPoints.capacity() && i < 500; i++) {
			sumOfAValue += keyPoints[i].pt.x;
		}
		if (keyPoints.capacity() < 500) {
			aValue = sumOfAValue / (keyPoints.capacity());
		}
		else {
			aValue = sumOfAValue / 500;
		}
	}
	catch (Exception e) {
		throw e;
	}
}

double Straight::getAValue() {
	return aValue;
}

double Straight::getBValue() {
	return bValue;
}