#include "..\Header Files\Source.h"

void drawHorizontalLine(Straight straight, Mat matImage) {
	double yValue;
	double prevYValue = straight.getBValue();
	for (int i = 1; i < matImage.cols; i++) {
		yValue = straight.getBValue();
		line(matImage, Point(i - 1, prevYValue), Point(i, yValue), CV_RGB(255, 25, 60), 20);
		prevYValue = yValue;
	}

	imwrite("C:/Users/B�a�ej/Documents/inzynierka/OpenCVProject/OpenCVProject/Images/wynikResultHorizontalHorizontalMatresultKeyPoints.bmp", matImage);
}

void drawVerticalLine(Straight straight, Mat matImage) {
	double XValue;
	double prevXValue = straight.getAValue();
	for (int i = 1; i < matImage.rows; i++) {
		XValue = straight.getAValue();
		line(matImage, Point(prevXValue, i - 1), Point(XValue, i), CV_RGB(255, 25, 60), 20);
		prevXValue = XValue;
	}

	imwrite("C:/Users/B�a�ej/Documents/inzynierka/OpenCVProject/OpenCVProject/Images/wynikResultVerticalVerticallMatresultKeyPoints.bmp", matImage);
}



int main()
{
	string table[3] = { "C:/Users/B�a�ej/Desktop/Studia/image/CAM00142.jpg","C:/Users/B�a�ej/Desktop/Studia/image/CAM00143.jpg", "C:/Users/B�a�ej/Desktop/Studia/image/CAM00141.jpg" };
	Mat firstImage = imread(table[0], COLOR_BGR2GRAY);
	Mat secondImage = imread(table[1], COLOR_BGR2GRAY);
	Mat resultCalibration;
	subtract(firstImage, secondImage, resultCalibration);

	Mat resultCalibrationGrayForVerticalLines;
	Mat resultCalibrationGrayForHorizontalLines;
	cvtColor(resultCalibration, resultCalibrationGrayForVerticalLines, COLOR_BGR2GRAY);
	resultCalibrationGrayForHorizontalLines = resultCalibrationGrayForVerticalLines.clone();

	ObjectProportion calibrationObjectProportion;
	calibrationObjectProportion.setLeftRightDistance(calibrationObjectProportion.detectVerticalLinesAndCalculateWidth(resultCalibrationGrayForVerticalLines));
	calibrationObjectProportion.setTopDownDistance(calibrationObjectProportion.detectHorizontalLinesAndCalculateHeight(resultCalibrationGrayForHorizontalLines));

	Mat thirdImage = imread(table[2], IMREAD_COLOR);
	Mat result;
	subtract(firstImage, thirdImage, result);

	Mat resultGrayForVerticalLines;
	Mat resultGrayForHorizontalLines;
	cvtColor(result, resultGrayForVerticalLines, COLOR_BGR2GRAY);
	resultGrayForHorizontalLines = resultGrayForVerticalLines.clone();
	

	ObjectProportion objectProportion;
	objectProportion.setLeftRightDistance(objectProportion.detectVerticalLinesAndCalculateWidth(resultGrayForVerticalLines));
	objectProportion.setTopDownDistance(objectProportion.detectHorizontalLinesAndCalculateHeight(resultGrayForHorizontalLines));

	cout << "Podaj wysoko�� w cm obiektu kalibruj�cego." << endl;
	double calibrationObjectWidth;
	cin >> calibrationObjectWidth;

	cout << "Podaj szeroko�� w cm obiektu kalibracyjnego." << endl;
	double calibrationObjectHeight;
	cin >> calibrationObjectHeight;

	double objectCalibrationWidthProportion = (calibrationObjectWidth) / calibrationObjectProportion.getLeftRightDistance();
	double objectCalibrationHeightProportion = (calibrationObjectHeight) / calibrationObjectProportion.getTopDownDistance();

	cout << "Szeroko�� mierzonego obiektu wynosi " << objectProportion.getLeftRightDistance() * objectCalibrationWidthProportion << " cm." << endl;
	cout << "Wysoko�� mierzonego obiektu wynosi " << objectProportion.getTopDownDistance() * objectCalibrationHeightProportion << " cm." << endl;

	return 0;
}


//int main(int, char**)
//{
//	VideoCapture cap(CV_CAP_ANDROID); // open the default camera
//	if (!cap.isOpened())  // check if we succeeded
//		return -1;
//
//	Mat edges;
//	namedWindow("edges", 1);
	//for (;;)
	//{
	//	Mat frame;
	//	cap >> frame; // get a new frame from camera
	//	Point2f src_center(frame.cols / 2.0F, frame.rows / 2.0F);
	//	Mat frameRotated = getRotationMatrix2D(src_center, 180, 1.0);
	//	warpAffine(frame, frame, frameRotated, frame.size());
	//	cvtColor(frame, edges, COLOR_BGR2GRAY);
	//	GaussianBlur(edges, edges, Size(7, 7), 1.5, 1.5);
	//	Canny(edges, edges, 0, 30, 3);
	//	imshow("edges", edges);
	//	if (waitKey(30) >= 0) break;
	//}
//	// the camera will be deinitialized automatically in VideoCapture destructor
//	return 0;
//}
